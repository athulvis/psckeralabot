#! /usr/bin/python3

import sqlite3

class DBUpdater:
    """
    Object that abstrcts the database for storing the title and check for duplication later
    It uses sqlite3 as the database engine
    """

    def __init__(self, db_name):
        self.db_name = db_name

    def db_create(self):
        """
        Create new table if not exist in the database, create database if not found vacancy
        """
        try:
            # Creates or opens SQLite3 Database
            conn = sqlite3.connect(self.db_name, check_same_thread=False)
            c = conn.cursor()
            c.execute('''CREATE TABLE IF NOT EXISTS OPPORTUNITIES(title TEXT PRIMARY KEY)''')
            status = True
        except sqlite3.Error as e:
            print(e.args[0])
            status= False
        finally:
            conn.close()
            return status

    def db_update(self,entry):
        """
        Update the database with new data, call this method with parsed entry
        from the web. If the method encounters duplication in the data raises
        an exception and returns a False flag otherwise a True flag

        :param entry: string containing title parsed from the Web
        :return status: Boolean value indicating the result of database operation
        """
        try:
            # Creates or opens SQLite3 Database
            conn = sqlite3.connect(self.db_name, check_same_thread=False)
            c = conn.cursor()
            c.execute('INSERT INTO OPPORTUNITIES(title) VALUES (?)', [entry])
            conn.commit()
            status= True
        except sqlite3.IntegrityError as e:
            print(e.args[0])
            status= False
        finally:
            conn.close()
            return status
