#! /usr/bin/python3

import urllib.request as req
from urllib.error import URLError, HTTPError
from bs4 import BeautifulSoup

class Extractor:
    def __init__(self):
        pass

class FeedExtractor(Extractor):
    """
    Object that abstracts the RSS/Atom feed. Validates and Fetches information
    from the provided URL
    """

    def __init__(self, url='http://inspirehep.net/rss?cc=Jobs&ln=en'):
        import feedparser
        self.url = url

    def validateURL(self):
        """
        Validate the URL and return Boolean status

        :return status: Boolean value indicating the result of validation of URL
        """
        #status = False
        try:
            self.feed = feedparser.parse(self.url)
            self.feed.entries[0]['title']
            status = True
        except IndexError:
            #return("ERROR: The link does not seem to be a RSS feed or is not supported")
            status = False
        finally:
            return status

    def fetchEntry(self):
        """
        Fetch RSS/Atom feed entry from the web and create a dictionary of title
        and description of each item

        :return entries: dictionary containing title and description of items
        """
        entries = dict()
        for entry in self.feed.entries:
            entries[entry.title] = entry.description
        return entries

class HTMLExtractor(Extractor):
    """
    Object that abstracts the HTML Web page containing the information required.
    Validates and Fetches information from the provided URL
    """
    def __init__(self, url = 'https://inspirehep.net/search?ln=en&cc=Jobs&p=&action_search=Search&rg=10'):

        self.url = url

    def validateURL(self):
        """
        Validate the URL and return Boolean status

        :return status: Boolean value indicating the result of validation of URL
        """
        try:
            self.page = req.urlopen(self.url)
            status = True
        except HTTPError as e:
            status = False
        except  URLError as e:
            status = False
        except Exception as e:
            print(e)
        finally:
            return status

    def fetchEntry(self):
        """
        Fetch HTML Web entry from the DOM and create a dictionary of title
        and description of each item

        :return entries: dictionary containing title and description of items
        """
        details = []
        psclatest ='https://www.keralapsc.gov.in/latest'
        header = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
        request=req.Request(psclatest,None,header)
        page = req.urlopen(request)
        soup = BeautifulSoup(page, 'html.parser')
        record1 = soup.find_all('td',attrs={'class':"views-field views-field-type"})
        record2 = soup.find_all('td',attrs={'class':"views-field views-field-title"})
        record3 = soup.find_all('td',attrs={'class':"views-field views-field-body"})
        record4 = soup.find_all('td',attrs={'class':"views-field views-field-field-file"})
        records = (record1,record2,record3,record4)
        for i in range(0,15):
            title = record2[i].text
            url = (record4[i].a).get('href')
            description = record3[i].text
            type = record1[i].text
            details.append((title, type, description, url))
        return details

def main():
    print("Running Test URL")
    ext = HTMLExtractor()
    if ext.validateURL():
        print(ext.fetchEntry())
    else:
        print("ERROR: The link does not seem to be a RSS feed or is not supported")

if __name__ == '__main__':
    main()
