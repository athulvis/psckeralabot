import logging
from telegram.ext import Updater, CommandHandler, Job
from WebExtractor import Extractor
from WebExtractor import DBUpdater
from telegram import ParseMode
from bs4 import BeautifulSoup as bs
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

Token = ""
chatid = ""
delay = 60
rss_dict = {}

class psckeralaBot:
    def __init__(self):
        pass

    def updateBot(self,update,context):
        ext=Extractor.HTMLExtractor()
        dbu=DBUpdater('pscdata.db')
        if ext.validateURL() and dbu.db_create():
            latestnews = ext.fetchEntry()
            for news in latestnews:
                print(news)
                if dbu.db_update(news[0]):
                    #title, type, description, url
                    txt = "*"+news[0] + "*\n\n*Type: *#" + news[1] + "\n\n*Description :*" + news[2] + "\n\n*Link :* https://keralapsc.gov.in" + news[3] 
                    try:
                        #for individual people chat_id = update.message.chat_id
                        context.bot.send_message(chat_id = '@k_psc', text = txt, parse_mode='Markdown')
                    except:
                        print(txt)
                    #print("Successfully updated database")
                else:
                    print("Database error occured")

def main():
    Bot = psckeralaBot()
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    updater = Updater(token=Token,use_context=True)
    job_queue = updater.job_queue
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("update", Bot.updateBot))
    job_queue.run_repeating(Bot.updateBot, delay)
    updater.start_polling()

if __name__ == '__main__':
    main()
